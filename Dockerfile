FROM openjdk:16-oracle
COPY build/libs/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
